# This system module is no longer maintained!

Forking DND5E worked for me in the short term, but I have moved on to including
the same changes in an Add-On Module instead. You can find that work here:

https://gitlab.com/csmcfarland/dme

# Dark Matter
This is a fork of the wonderful [DnD5e system module](https://gitlab.com/foundrynet/dnd5e) for [Foundry Virtual Tabletop](http://foundryvtt.com).

# Foundry Virtual Tabletop - DnD5e Game System

This game system for [Foundry Virtual Tabletop](http://foundryvtt.com) provides character sheet and game system
support for the Fifth Edition of the world's most popular roleplaying game.

This system is offered and may be used under the terms of the Open Gaming License v1.0a and its accompanying
[Systems Reference Document 5.1 (SRD5)](http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf).

This system provides character sheet support for Actors and Items, mechanical support for dice and rules necessary to
play games of 5th Edition, and compendium content for Monsters, Heroes, Items, Spells, Class Features, Monster
Features, and more!

The software component of this system is distributed under the GNUv3 license.

## Installation Instructions

This is a fork of the DnD5e system module, and it cannot co-exist with that
module in a Foundry VTT install due to it sharing the same underlying module
name.

To install and use the DnD5e system for Foundry Virtual Tabletop, simply paste the following URL into the
**Install System** dialog on the Setup menu of the application.

https://gitlab.com/foundrynet/dnd5e/raw/master/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/dnd5e`` folder. You
may do this by cloning the repository or downloading a zip archive from the
[Releases Page](https://gitlab.com/foundrynet/dnd5e/-/releases).

## Community Contribution

Code and content contributions are accepted. Please feel free to submit issues to the issue tracker or submit merge
requests for code changes. Approval for such requests involves code and (if necessary) design review by Atropos. Please
reach out on the Foundry Community Discord with any questions.
